# Next SSR + Auth
Projet NextJS lié à un backend en Symfony avec utilisation du SSR et d'une authentification avec du JWT

## How To use
1. Cloner le projet
2. `npm install`
3. Modifier le `NEXT_PUBLIC_SERVER_URL` dans les .env ou .env.local si jamais le serveur n'est pas sur le localhost:8000
4. Lancer le serveur Symfony

## Authentification JWT
(Les différents composants nécessaire à l'authentification ont été regroupés dans le dossier `src/auth`)

Le flow d'authentification est le suivant 
1. Valider un formulaire de login en envoyant identifiant et mot de passe vers le back
2. Le back valide ces credentials et renvoie un JWT dans une réponse HTTP
3. Côté front on stock le JWT dans des cookies/localstorage/autre, quelque part où l'on pourra y accéder après un rechargement de page.
4. On stock également le token (ou le user si on l'a sous la main) dans un useState global de React afin de pouvoir modifier l'affichage en fonction de si l'on est connecté ou non
5. Toutes les requêtes Axios futures contiendront le JWT afin d'être authentifier par le backend et que celui ci nous donne accès ou non aux ressources requêtées
6. Pour se déconnecter, on vide le state contenant le token ainsi que le cookie/localstorage

### Requête de connexion
Côté service ou ailleurs, on configure une requête vers la route de login de notre backend. Ici on utilise un email et un password pour la connexion, si on a choisi autre chose qu'un email comme identifiant, on modifie l'objet envoyé dans la requête.
```ts
export async function login(email:string,password:string) {
    const response = await axios.post<{token:string}>('/api/login', {email, password});
    return response.data.token;
}
```

### Le state global
Afin de pouvoir accéder au status de connexion dans n'importe quel component ou page de l'application sans avoir besoin de le passer en props dans tous les sens (props drilling), on crée un state global à l'aide des React Context qui permettent d'accéder à des valeurs depuis n'importe quel component

```tsx
interface AuthState {
    token?: string | null;
    setToken: (value: string | null) => void
}
export const AuthContext = createContext({} as AuthState);

export const AuthContextProvider = ({ children }: any) => {
    const [token, setToken] = useState<string | null>('');

    function handleSet(value: string | null) {
        if (value) {
            setCookie(null, 'token', value)
        } else {
            destroyCookie(null, 'token');
        }
        setToken(value);
    }

    useEffect(() => {
        setToken(parseCookies().token);

    }, []);

    return (
        <AuthContext.Provider value={{ token, setToken: handleSet }}>
            {children}
        </AuthContext.Provider>
    );
}
```
Histoire de tout regrouper, on gère également le fait de stocker le token en cookies (ici en utilisant la library nookies qui simplifie l'utilisation de ceux ci avec nextjs).

On pourrait rajouter d'autres informations dans ce context (par exemple si on souhaitait stocker les informations du user plutôt que juste son token), il suffirait de rajouter les propriétés dans l'interface AuthState, de créer un autre useState et de le passer aux value du AuthContext.Provider

Pour que ce Context fonctionne et soit disponible dans les component, il faut entouré les composants ciblés par le provider, chose que l'on fait dans le _app.tsx afin de le rendre globalement accessible

```tsx
export default function App({ Component, pageProps }: AppProps) {


  return (
    <AuthContextProvider>
          <Component {...pageProps} />
    </AuthContextProvider>
  )
}
```

### Connexion
On crée un formulaire de connexion qu'on liera à notre fonction login définie plus haut. Si cette requête a une erreur on indique un message correspondant, sinon on récupère le token qu'on stock via notre AuthContext
```ts
 const {setToken} = useContext(AuthContext);

const [error, setError] = useState('');

async function handleSubmit(values: User) {
    setError('');
    try {
        setToken(await login(values.email, values.password));

        router.back();
    }catch(error:any) {
        if(error.response?.status == 401) {
            setError('Invalid login/password');

        } else {
            setError('Server error');
        }
    }
}
```

### Déconnexion
Pour la déconnexion, il suffit de faire un setToken sur notre AuthContext en lui donnant une valeur null
```tsx
<a className='item' onClick={() => setToken(null)}>Logout</a>
```

### Injecter le token dans les requêtes
Afin d'ajouter le JWT à toutes les requêtes faites avec axios, on utilise un intercepteur, qui comme son nom l'indique interceptera les requêtes et permettra de les modifier, ici en ajoutant le token dans les headers :

```ts
axios.interceptors.request.use((config) => {
    const { token } = parseCookies();
    if (token) {
        config.headers.setAuthorization('Bearer ' + token);
    }
    return config;
})
```
Cet intercepteur devra être lancé en même temps que l'application, dans le _app.tsx
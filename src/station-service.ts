import axios from "axios";
import { Bike, Station } from "./entities";


export async function fetchAllStation() {
    const response = await axios.get<Station[]>('/api/station');
    return response.data;
}


export async function fetchOneStation(id:number|string) {
    const response = await axios.get<Station>('/api/station/'+id);
    return response.data;
}


export async function fetchStationBikes(id:number|string) {
    const response = await axios.get<Bike[]>(`/api/station/${id}/bikes`);
    return response.data;
}
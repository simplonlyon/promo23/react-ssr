import { Rental } from "@/entities";
import { List } from "antd";
import dayjs from "dayjs";


interface Props {
    rentals:Rental[]
}
export default function RentalList({rentals}:Props) {

    function rentalDuration(rental:Rental) {
        const start = dayjs(rental.endTime);
        
        return dayjs.duration(start.diff(rental.startTime)).format('HH:mm:ss');
    }   

    return (
        <List
            header={<h2>Rental History</h2>}
            bordered={true}
            dataSource={rentals}
            renderItem={(item) =>
                <p>{item.startStation.label} to {item.returnStation?.label} : {rentalDuration(item)}</p>
            }
        />
    )
}
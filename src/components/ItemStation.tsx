import { Station } from "@/entities"
import { green, red } from "@ant-design/colors";
import { Badge, Card } from "antd";
import Link from "next/link";


interface Props {
    station: Station;
}

export default function ItemStation({ station }: Props) {

    return (
        <Card title={station.label}
            extra={
                    <Link href={'/station/' + station.id}>See bikes</Link>
            }>
            <p>Latitude : {station.latitude}</p>
            <p>Longitude : {station.longitude}</p>
            <p>Bikes : 
                <Badge count={station.bikeCount + '/' + station.slots} color={station.bikeCount > 0 ? green.primary:red.primary} />
            </p>
        </Card>
    )
}
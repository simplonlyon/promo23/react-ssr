import { AuthContext } from "@/auth/auth-context";
import { Bike } from "@/entities"
import { red } from "@ant-design/colors";
import { ThunderboltFilled, ToolFilled } from "@ant-design/icons";
import { Button, List } from "antd";
import { useContext } from "react";

interface Props {
    bike: Bike;
    onRentClick: () => void
}

export default function ListItemBike({ bike, onRentClick }: Props) {
    const {token} = useContext(AuthContext);

    return (
        <List.Item>
            <List.Item.Meta title={'Bike ' + bike.id} />
            <div>
                {bike.electric && <ThunderboltFilled />}
                {bike.broken && <ToolFilled style={{color: red.primary}} />}
                {token && <Button onClick={onRentClick}>Rent</Button>}
            </div>
        </List.Item>
    )
}
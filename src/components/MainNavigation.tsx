import { AuthContext } from '@/auth/auth-context';
import { Menu } from 'antd';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { MenuProps } from 'rc-menu';
import { useContext } from 'react';


export default function MainNavigation() {
    const { token, setToken } = useContext(AuthContext);
    const router = useRouter();
    

    const menuItems:MenuProps['items'] = [
        {
            label: <Link href="/">Home</Link>,
            key: '/',
            
        }
    ];
    if(token) {
        menuItems.push(
            {label: <Link href="/rental">Rental</Link>, key: '/rental'},
            {label: <Link href="/account">Account</Link>, key: '/account'},
            {label: 'Logout', key: '/logout', onClick:() => setToken(null)}
        )
    } else {
        menuItems.push({
            label: <Link href="/login">Login</Link>,
            key: '/login'
        });
    }



    return (

        <Menu selectedKeys={[router.route]} items={menuItems} mode="horizontal">
        </Menu>
    )
}
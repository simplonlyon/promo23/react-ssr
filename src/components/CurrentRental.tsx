import { Rental } from "@/entities"
import { Card } from "antd";
import dayjs from "dayjs";
import { useEffect, useState } from "react";

interface Props {
    rental: Rental;
}

export default function CurrentRental({ rental }: Props) {
    const [now, setNow] = useState(dayjs());

    useEffect(() => {
        const interval = setInterval(() => {
            setNow(dayjs());
        }, 1000);
        return () => clearInterval(interval);
    }, []);

    function rentalDuration() {

        return dayjs.duration(now.diff(rental.startTime)).format('HH:mm:ss');
    }
    return (

        <Card>
            <p>Started from  {rental.startStation.label} station</p>
            <p>Rental duration : {rentalDuration()}</p>

        </Card>
    )
}
export interface Station {
    id?: number;
    label: string;
    slots: number;
    latitude?: number;
    longitude?: number;
    bikeCount: number;
}

export interface Bike {
    id?:number
    broken:boolean;
    electric:boolean;
    station?:Station;
}

export interface Rental {
    id?:number;
    startTime:string;
    endTime?:string;
    startStation:Station;
    returnStation?:Station;
    bike:Bike;
}

export interface User {
    id?:number;
    email:string;
    password:string;
    role?:string;
}
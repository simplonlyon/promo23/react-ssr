import 'antd/dist/reset.css'
import '@/styles/globals.css'
import '../auth/axios-config';
import type { AppProps } from 'next/app'
import { Layout } from 'antd';
import MainNavigation from '@/components/MainNavigation';
import { AuthContextProvider } from '@/auth/auth-context';
import dayjs from 'dayjs';
import duration from 'dayjs/plugin/duration';

dayjs.extend(duration);


export default function App({ Component, pageProps }: AppProps) {


  return (
    <AuthContextProvider>

      <Layout>
        <Layout.Header  style={{padding: 0}}>
          <MainNavigation />
        </Layout.Header>
        <Layout.Content>
          <Component {...pageProps} />
        </Layout.Content>
      </Layout>
    </AuthContextProvider>
  )
}

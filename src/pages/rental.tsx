
import CurrentRental from "@/components/CurrentRental";
import RentalList from "@/components/RentalList";
import { Rental } from "@/entities"
import { fetchCurrentRental, fetchRentalHistory } from "@/rental-service";
import { Button } from "antd";
import { useEffect, useState } from "react"


export default function rental() {
    const [current, setCurrent] = useState<Rental>();
    const [history, setHistory] = useState<Rental[]>();


    useEffect(() => {

        fetchCurrentRental()
            .then(data => setCurrent(data))
            .catch(() => {});
    }, []);

    async function getHistory() {
        const data = await fetchRentalHistory();
        setHistory(data);
    }


    return (
        <>
            {current ?
                <CurrentRental rental={current} />:
                <div>No current rental</div>
            }
            <Button onClick={getHistory}>See Rental History</Button>
            {history  && 
                <RentalList rentals={history} />
            }
        </>
    )
}
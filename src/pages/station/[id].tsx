import ListItemBike from "@/components/ListItemBike";
import { Bike, Station } from "@/entities"
import { postRent } from "@/rental-service";
import { fetchOneStation, fetchStationBikes } from "@/station-service"
import { List } from "antd";
import { GetServerSideProps } from "next"
import { useState } from "react";

interface Props {
    station: Station
    bikes: Bike[];
}

export default function OneStation({ station, bikes:bikesProps }: Props) {
    const [bikes,setBikes] = useState(bikesProps);

    async function rentBike(bike:Bike) {
        await postRent(bike);
        setBikes(bikes.filter(item => item.id != bike.id));
    }

    return (
        <>
            <h1>Station : {station.label}</h1>


            <List
                header={<h2>Bike List</h2>}
                bordered={true}
                dataSource={bikes}
                renderItem={(item) => 
                    <ListItemBike bike={item} onRentClick={() => rentBike(item)} />
                }
            />
        </>
    )
}

export const getServerSideProps: GetServerSideProps<Props> = async (context) => {
    const { id } = context.query;

    try {

        return {
            props: {
                station: await fetchOneStation(Number(id)),
                bikes: await fetchStationBikes(Number(id))
            }
        }

    } catch {
        return {
            notFound: true
        }
    }

}
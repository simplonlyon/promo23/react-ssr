import { AuthContext } from "@/auth/auth-context";
import { login } from "@/auth/auth-service";
import { User } from "@/entities";
import { red } from "@ant-design/colors";
import { Button, Card, Form, Input } from "antd";
import { useRouter } from "next/router";
import { useContext, useState } from "react";



export default function loginPage() {
    const router = useRouter();
    const {setToken} = useContext(AuthContext);

    const [error, setError] = useState('');

    async function handleSubmit(values: User) {
        setError('');
        try {
            setToken(await login(values.email, values.password));

            router.back();
        }catch(error:any) {
            if(error.response?.status == 401) {
                setError('Invalid login/password');

            } else {
                setError('Server error');
            }
        }
    }

    return (
        <Card style={{ width: '75%', margin: 'auto', marginTop: '20px' }}>
            <h1>Login</h1>
            {error && <p style={{color:red.primary}}>{error}</p>}
            <Form
                onFinish={handleSubmit}
            >
                <Form.Item
                    label="Email"
                    name="email"
                    rules={[{ required: true, message: 'Please input your email!' }]}
                >
                    <Input type="email" />
                </Form.Item>

                <Form.Item
                    label="Password"
                    name="password"
                    rules={[{ required: true, message: 'Please input your password!' }]}
                >
                    <Input.Password />
                </Form.Item>

                <Form.Item>
                    <Button type="primary" htmlType="submit">
                        Login
                    </Button>
                </Form.Item>
            </Form>
        </Card>
    )
}
import { AuthContext } from "@/auth/auth-context";
import ItemStation from "@/components/ItemStation";
import { Station } from "@/entities";
import { fetchAllStation } from "@/station-service";
import { Col, Row } from "antd";
import { GetServerSideProps } from "next";
import { useContext } from "react";

interface Props {
  stations: Station[];
}

export default function Home({ stations }: Props) {
  return (
    <>
      <Row gutter={[10, 10]} justify="center">
        {stations.map(item =>
          <Col xs={12} md={6} key={item.id} >
            <ItemStation station={item} />
          </Col>
        )}
      </Row>
    </>
  );
}

/**
 * Next
 */
export const getServerSideProps: GetServerSideProps<Props> = async () => {

  return {
    props: {
      stations: await fetchAllStation() //fait un fetch du serveur Symfony

    }
  }
}
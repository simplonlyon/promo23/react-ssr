import { AuthContext } from "@/auth/auth-context";
import { fetchUser } from "@/auth/auth-service";
import { User } from "@/entities"
import { useRouter } from "next/router";
import { useContext, useEffect, useState } from "react"



export default function account() {
    const [user, setUser] = useState<User>();
    const router = useRouter();


    useEffect(() => {
        fetchUser().then(data => {
            setUser(data);
        }).catch(error => {
            if(error.response.status == 401) {
                router.push('/login')
            }
        });
    }, [])

    return (
        <>
            <h1>Account</h1>

            <p>email : {user?.email}</p>
            <p>password : {user?.password}</p>
            <p>role : {user?.role}</p>
        </>
    )
}
import axios from "axios";
import { Bike, Rental } from "./entities";



export async function postRent(bike:Bike) {
    const response = await axios.post<Rental>('/api/rental/'+bike.id);
    return response.data;
}

export async function fetchCurrentRental() {
    const response = await axios.get<Rental>('/api/rental/current');
    return response.data;
}

export async function fetchRentalHistory() {
    const response = await axios.get<Rental[]>('/api/rental');
    return response.data;
}
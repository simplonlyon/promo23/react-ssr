import { User } from "@/entities";
import axios from "axios";



export async function login(email:string,password:string) {
    const response = await axios.post<{token:string}>('/api/login', {email, password});
    return response.data.token;
}

export async function fetchUser() {
    const repsonse = await axios.get<User>('/api/account');
    return repsonse.data;
}